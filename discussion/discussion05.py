from functools import reduce

def apply_to_all(fn, seq):
    return [fn(i) for i in seq]


# 1 List Comprehension
# 1. What would Python print?
"""
>>> [i + 1 for i in [1, 2, 3, 4, 5] if i % 2 == 0]
[3, 5]
>>> [(lambda j: j * j)(2 * i) for i in [5, -1, 3, -1, 3] if i > 2]
[100, 36, 36]
>>> [[y * 2 for y in [x, x + 1]] for x in [1, 2, 3, 4]]
[[2, 4], [4, 6], [6, 8], [8, 10]]
"""

# 2. Define a function foo that takes in a list lst and returns a new list that keeps only
#    the even-indexed elements of lst and multiples each of those elements by the corresponding index.
def foo(lst):
    """
    >>> x = [1, 2, 3, 4, 5, 6]
    >>> foo(x)
    [0, 6, 20]
    """
    return [lst[index] * index for index in range(len(lst)) if index % 2 == 0]


# 2 Trees
def is_leaf(tree):
    return type(tree) != list

def apply_to_leaves(map_fn, tree):
    if is_leaf(tree):
        return map_fn(tree)
    else:
        return [apply_to_leaves(map_fn, branch) for branch in tree]

# 2.2 Questions
# 1. Define a function square tree(tree) that uses apply to leaves to square every
# item in tree. You can assume that every item is a number.
def square_tree(tree):
    """Returns a tree containing integers by squaring all of
    TREE's elements
    >>> square_tree([1, [2, [3]], 4])
    [1, [4, [9]], 16]
    """
    return apply_to_leaves(lambda x: x * x, tree)


# 2.
def height(tree):
    """Returns the height of a tree
    >>> height(1)
    0
    >>> height([1])
    1
    >>> height([1, [2, [3]], [4, [5]]])
    3
    """
    if is_leaf(tree):
        return 0
    else:
        return max([height(branch) + 1 for branch in tree])

# 3. In linguistics, sentences are parsed according to a grammar into a tree, such as the one
# below. Write a function join, which takes in a tree consisting of strings in its leaves
# and turns it back into a sentence.
def join(tree):
    """ Joins a tree containing strings as leaves into a sentence.
    >>> join('hi')
    'hi'
    >>> join(['hello', ' ', 'world'])
    'hello world'
    >>> join([['the', 'cat'], ['sat', ['on', ['the', 'mat']]]])
    'thecatsatonthemat'
    """
    if is_leaf(tree):
        return tree
    else:
        return ''.join([join(branch) for branch in tree])

# 4. Define a function search(tree, x) that searches a tree for leaves of value x. search
# should return the minimum depth of all such leaves, or False if no leaves are found.
def search(tree, x):
    """Returns the minimum depth of a leaf with value X in TREE, False if not found.
    >>> search([2, [3, 4], [1, [5, [3]]]], 3)
    2
    >>> search([2, [3, 4], [1, [5, [3]]]], 6)
    False
    """
    if is_leaf(tree):
        if tree == x:
            return 0
        else:
            return False
    else:
        branches = [(search(branch, x) + 1) for branch in tree if search(branch, x) is not False]
        return len(branches) > 0 and min(branches)


# 2.3 Rooted Trees

def rooted(value, branches):
    return [value] + list(branches)

def root(tree):
    return tree[0]

def branches(tree):
    return tree[1:]

def leaf(value):
    return rooted(value, [])

def is_rooted_leaf(tree):
    return branches(tree) == []

from operator import add, mul
def eval_tree(tree):
    """Evaluates an expression tree with functions as root
    >>> eval_tree(leaf(1))
    1
    >>> expr = rooted(mul, [leaf(2), leaf(3)])
    >>> eval_tree(expr)
    6
    >>> eval_tree(rooted(add, [expr, leaf(4)]))
    10
    """
    if is_rooted_leaf(tree):
        return root(tree)
    else:
        if root(tree) is add:
            return reduce(add, [eval_tree(branch) for branch in branches(tree)], 0)
        else:
            return reduce(mul, [eval_tree(branch) for branch in branches(tree)], 1)


def hailstone_tree(n, h):
    """Generates a rooted tree of hailstone numbers that
    will reach N, with height H.
    >>> hailstone_tree(1, 0)
    [1]
    >>> hailstone_tree(1, 4)
    [1, [2, [4, [8, [16]]]]]
    >>> hailstone_tree(8, 3)
    [8, [16, [32, [64]], [5, [10]]]]
    """
    if h == 0:
        return leaf(n)
    else:
        if ((n - 1) % 3 == 0) and ((n - 1) // 3 > 1):
            return rooted(n, [hailstone_tree(n * 2, h - 1), hailstone_tree((n - 1) // 3, h - 1)])
        else:
            return rooted(n, [hailstone_tree(n * 2, h - 1)])


def find_path(tree, x):
    """ Returns a path in a tree to a leaf with value X,
    False if such a leaf is not present.
    >>> r, l = rooted, leaf
    >>> t = r(2, [r(7, [l(3), r(6, [l(5), l(11)])]), l(15)])
    >>> find_path(t, 5)
    [2, 7, 6, 5]
    >>> find_path(t, 6)
    [2, 7, 6]
    >>> find_path(t, 10)
    False
    """
    if root(tree) == x:
        return [root(tree)]
    elif is_rooted_leaf(tree):
        return False
    else:
        for branch in branches(tree):
            path = find_path(branch, x)
            if path is not False:
                return [root(tree)] + path

        return False
