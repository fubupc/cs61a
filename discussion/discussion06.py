"""
1.
>>> lst1 = [1, 2, 3]
>>> lst2 = lst1
>>> lst2 is lst1
True

2. 
>>> lst1.append(4)
>>> lst1
[1, 2, 3, 4]

3.
>>> lst2
[1, 2, 3, 4]

4.
>>> lst2[1] = 42
>>> lst2
[1, 42, 3, 4]

5.
>>> lst1
[1, 42, 3, 4]

6.
>>> lst1 = lst1 + [5]
>>> lst1
[1, 42, 3, 4, 5]

7.
>>> lst2
[1, 42, 3, 4]

8.
>>> lst2 is lst1
False
"""


################################
# 2.1 List Mutation Questions  #
################################

# 1. Write a function that removes all instances of el from lst.
def remove_all(el, lst):
    """Removes all instances of el from lst.
    >>> x = [3, 1, 2, 1, 5, 1, 1, 7]
    >>> remove_all(1, x)
    >>> x
    [3, 2, 5, 7]
    """
    try:
        while True:
            lst.remove(el)
    except Exception as e:
        pass

# 2. Write a function square elements which takes a lst and replaces each element
# with the square of that element. Mutate lst rather than returning a new list.
def square_elements(lst):
    """Squares every element in lst.
    >>> lst = [1, 2, 3]
    >>> square_elements(lst)
    >>> lst
    [1, 4, 9]
    """
    for i in range(len(lst)):
        lst[i] = lst[i] * lst[i]



################################
# 2.2 Extra Practice           #
################################

# 1. Write a function which takes in a list lst, and two values x and y, and adds as many
# ys to the end of lst as there are xs. Do not use the count list method.
def add_this_many(x, y, lst):
    """Adds y to the end of lst the number of times x occurs.
    >>> lst = [1, 2, 4, 2, 1]
    >>> add_this_many(2, 5, lst)
    >>> lst
    [1, 2, 4, 2, 1, 5, 5]
    """
    while x > 0:
        lst.append(y)
        x -= 1

# 2. Write a function which reverses a list using mutation. Don’t use the reverse list method.
def reverse_list(lst):
    """Reverses lst in-place (mutating the original list).
    >>> lst = [1, 2, 3, 4]
    >>> reverse_list(lst)
    >>> lst
    [4, 3, 2, 1]
    >>> pi = [3, 1, 4, 1, 5]
    >>> reverse_list(pi)
    >>> pi
    [5, 1, 4, 1, 3]
    """
    length = len(lst)
    for i in range(length // 2):
        lst[i], lst[length - i - 1] = lst[length - i - 1], lst[i]


################################
# 3 Dictionaries               #
################################

"""
>>> pokemon = {'pikachu': 25, 'dragonair': 148, 'mew': 151}
>>> pokemon['pikachu']
25
>>> pokemon['jolteon'] = 135
>>> pokemon
{'jolteon': 135, 'pikachu': 25, 'dragonair': 148, 'mew': 151}
>>> pokemon['ditto'] = 25
>>> pokemon
{'jolteon': 135, 'pikachu': 25, 'dragonair': 148, 'ditto': 25, 'mew': 151}
>>> 'mewtwo' in pokemon
False
>>> len(pokemon)
5
>>> pokemon['ditto'] = pokemon['jolteon']
>>> pokemon[('diglett', 'diglett', 'diglett')] = 51
>>> pokemon[25] = 'pikachu'
>>> pokemon
{'jolteon': 135, 'pikachu': 25, 'dragonair': 148, 'ditto': 135, 'mew': 151, 25: 'pikachu', ('diglett', 'diglett', 'diglett'): 51}
>>> pokemon['mewtwo'] = pokemon['mew'] * 2
>>> pokemon
{'jolteon': 135, 'pikachu': 25, 'dragonair': 148, 'ditto': 135, 'mew': 151, 25: 'pikachu', ('diglett', 'diglett', 'diglett'): 51, 'mewtwo': 302}
"""

# 1. Given a dictionary d, replace all occurences of x as a value (not a key) with y.
def replace_all(d, x, y):
    """
    >>> d = {'foo': 2, 'bar': 3, 'garply': 3, 'xyzzy': 99}
    >>> replace_all(d, 3, 'poof')
    >>> d
    {'foo': 2, 'bar': 'poof', 'garply': 'poof', 'xyzzy': 99}
    """
    for k, v in d.items():
        if x == v:
            d[k] = y

# 2.1. Given an arbitrarily deep dictionary d, replace all occurences of x as a value (not a key)
# with y. Hint: You will need to combine iteration and recursion.
def replace_all(d, x, y):
    """
    >>> d = {1: {2: 3, 3: 4}, 2: {4: 4, 5: 3}}
    >>> replace_all(d, 3, 1)
    >>> d
    {1: {2: 1, 3: 4}, 2: {4: 4, 5: 1}}
    """
    for k, v in d.items():
        if x == v:
            d[k] = y
        elif type(v) == dict:
            replace_all(v, x, y)


# 2. Given a (non-nested) dictionary d, write a function which deletes all occurrences of x
# as a value. You cannot delete items in a dictionary as you are iterating through it.
def remove_all(d, x):
    """
    >>> d = {1:2, 2:3, 3:2, 4:3}
    >>> remove_all(d,2)
    >>> d
    {2: 3, 4: 3}
    """
    to_delete = []
    for k, v in d.items():
        if x == v:
            to_delete.append(k)

    for k in to_delete:
        del d[k]

