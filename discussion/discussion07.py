# 1.2

class Cat(Pet):
    def __init__(self, name, owner, lives=9):
        Pet.__init__(self, name, owner)
        self.lives = lives

    def talk(self):
        """A cat says meow! when asked to talk."""
        print('meow!')

    def lose_life(self):
        """A cat can only lose a life if they have at least
        one life. When lives reach zero, the ’is_alive’
        variable becomes False.
        """
        if self.lives > 0:
            self.lives -= 1

        if self.lives == 0:
            self.is_live = False

"""
>>> class Foo(object):
...     def __init__(self, a):
...         self.a = a
...     def garply(self):
...         return self.baz(self.a)
>>> class Bar(Foo):
...     a = 1
...     def baz(self, val):
...         return val
>>> f = Foo(4)
>>> b = Bar(3)
>>> f.a
4
>>> b.a
3
>>> f.garply()
ERROR
>>> b.garply()
3
>>> b.a = 9
>>> b.garply()
9
>>> f.baz = lambda val: val * val
>>> f.garply()
16
"""


class NoisyCat(Cat):
    """A class that behaves just like a Cat, but always
    repeats things twice.
    """
    def talk(self):
        """A NoisyCat will always repeat what he/she said
        twice.
        """
        Cat.talk(self) 
        Cat.talk(self) 

# 2.2

class Vector:
    def __init__(self, vector):
        self.vector = vector

    def __getitem__(self, n):
        return self.vector[n]

    def __len__(self) :
        return len(self.vector)

    def __neg__ (self) :
        return Vector([- i for i in self])

    def __add__ (self, other):
        assert type(other) == Vector, "Invalid operation!"
        assert len(self) == len(other), "Invalid dimensions!"
        return Vector([ (self[i] + other[i]) for i in range(len(self))])

    def __sub__(self, other):
        return self.__add__(-other)

    def __mul__(self, other):
        if type(other) == int or type(other) == float:
            return Vector([i * other for i in self])
        elif type(other) == Vector:
            assert len(self) == len(other), "Invalid dimensions!"
            return sum([self[i] * other[i] for i in range(len(self))])

    def __rmul__(self, other):
        return self.__mul__(other)


from math import sqrt
def vector_length(v):
    return sqrt(v * v)

def normalize(v):
    return v * (1 / vector_length(v))

def proj(u, v):
    return v * (u * v) * (1 / (v * v))

