# coding=utf-8

######################################################
# basic
######################################################
class Tree:
    """A tree with entry as its root value."""
    def __init__(self, entry, branches=()):
        self.entry = entry
        self.parent = None

        for branch in branches:
            assert isinstance(branch, Tree)
            branch.parent = self

        self.branches = list(branches)

    def __repr__(self):
        """
        >>> Tree(1, [Tree(2, [Tree(3)]), Tree(4), Tree(5)])
        Tree(1, [Tree(2, [Tree(3)]), Tree(4), Tree(5)])
        """
        string = 'Tree(' + str(self.entry)
        if not self.branches:
            return string + ')'
        else:
            string += ', ['
            for branch in self.branches:
                string += repr(branch) + ', '
            string = string[:-2] + '])'
        return string

        # better: using format of list itself.
        if self.branches:
            return 'Tree({0}, {1})'.format(repr(self.entry), repr(self.branches))
        else:
            return 'Tree({0})'.format(repr(self.entry))

    def is_leaf(self):
        return not self.branches


#######
# 1.2 #
#######

# Define a function square tree(t) that squares every item in t. You can assume
# that every item is a number.
def square(x):
    return x * x

def square_tree(t):
    """Mutates a Tree t by squaring all its elements"""
    t.entry = square(t.entry)

    for branch in t.branches:
        square_tree(branch)


# Define a function make even which takes in a tree of integers, and mutates the tree
# such that all the odd numbers are increased by 1 and all the even numbers remain the
# same. Then write this function so that it returns a new tree instead.
def is_odd(n):
    return n % 2 != 0

def make_even(t):
    """
    >>> t = Tree(1, [Tree(2, [Tree(3)]), Tree(4), Tree(5)])
    >>> make_even(t)
    >>> t # Assuming __repr__ is defined
    Tree(2, [Tree(2, [Tree(4)]), Tree(4), Tree(6)])
    """
    if is_odd(t.entry):
        t.entry += 1

    for branch in t.branches:
        make_even(branch)

# 3. Assuming that every item in t is a number, let’s define average(t), which returns
# the average of all the items in t. 
def average(t):
    """
    >>> t = Tree(1, [Tree(2, [Tree(3)]), Tree(4), Tree(5)])
    >>> average(t)
    3.0
    """
    def sum_tree(t):
        if t.is_leaf():
            return [t.entry, 1]
        else:
            s = [t.entry, 1]
            for branch in t.branches:
                bs = sum_tree(branch)
                s[0] += bs[0]
                s[1] += bs[1]
            return s

    s = sum_tree(t)
    return s[0] / s[1]




# 4. Define the procedure find path that, given an Tree t and an entry entry, returns a
# list containing the nodes along the path required to get from the root of t to entry.
# If entry is not present in t, return False.

# a. Assume that the elements in t are unique. Find the path to an element.
# For instance, for the following tree, find path should return:
def find_path(t, entry):
    """
    >>> tree_ex = Tree(2, [Tree(7, [Tree(3), Tree(6, [Tree(5), Tree(11)])]), Tree(1)])
    >>> find_path(tree_ex, 5)
    [2, 7, 6, 5]
    """
    if t.entry == entry:
        return [t.entry]

    if t.is_leaf():
        return False

    for branch in t.branches:
        path = find_path(branch, entry)
        if path:
            return [t.entry] + path

    return False


# b. Now assume that the elements of the tree might not be unique. How would you
# change your answer from part a to find the shortest path? Try to implement the
# function find shortest, which has the same parameters as find path.
def find_shortest(t, entry):
    """
    >>> tree_ex = Tree(2, [Tree(7, [Tree(3), Tree(1)]), Tree(1, [Tree(3), Tree(5)])])
    >>> find_shortest(tree_ex, 1)
    [2, 1]
    >>> find_shortest(tree_ex, 5)
    [2, 1, 5]
    >>> find_shortest(tree_ex, 3)
    [2, 7, 3]
    """

    if t.entry == entry:
        return [t.entry]

    if t.is_leaf():
        return False

    shortest = None
    for branch in t.branches:
        path = find_shortest(branch, entry)
        if path and ((shortest is None) or (len(path) < len(shortest))):
            shortest = path

    if shortest:
        return [t.entry] + shortest
    else:
        return False


# 5.
# b. Now write a method first to last for the Tree class that swaps a tree’s own
# first child with the last child of other.
def first_to_last(self, other):
    my_first = self.branches[0]
    other_last = other.branches[-1]

    other_last.parent = self
    my_first.parent = other

    self.branches[0] = other_last
    other.branches[-1] = my_first


################################################
# Binary Tree
################################################
class BinaryTree(Tree):
    empty = Tree(None)
    empty.is_empty = True

    def __init__(self, entry, left=empty, right=empty):
        for branch in (left, right):
            assert isinstance(branch, BinaryTree) or branch.is_empty
        Tree.__init__(self, entry, (left, right))
        self.is_empty = False

    @property
    def left(self):
        return self.branches[0]

    @property
    def right(self):
        return self.branches[1]


# 1.5
# Q: What is the purpose of the assert statement in the second line of init ? Why
#    must we write this line explicitly instead of relying on Tree. ’s assert?
# A: branch of BinaryTree must be BinaryTree(or empty) as well while Tree.init allow non-BinaryTree.

# Q: Summarize the process of creating a new BinaryTree. How does Tree. init contribute? 
# A: 1. create a empty BinaryTree object.
#    2. call BinaryTree.__init__ on object.
#    3. call Tree.__init__ on object
#    4. set is_empty attribute on object.

# Q: Why do we use @property instead of writing self.left = self.branches[0] in init ?
# A: keep sync. (in case Tree's branches mutated.)



##############################
# Order of growth
##############################

# What is the order of growth in time for the following functions?

# 1
def factorial(n):
    if n == 0:
        return 1
    return n * factorial(n - 1)
# growth: n

def sum_of_factorial(n):
    if n == 0:
        return 1
    else:
        return factorial(n) + sum_of_factorial(n - 1)
# growth: n^2

# 2
def fib_iter(n):
    prev, cur, i = 0, 1, 1
    while i < n:
        prev, curr = curr, prev + curr
        i += 1
    return curr
# growth: n

# 3
def mod_7(n):
    if n % 7 == 0:
        return 0
    else:
        return 1 + mod_7(n - 1)
# growth: 1

# 4
def bar(n):
    if n % 2 == 1:
        return n + 1
    return n

def foo(n):
    if n < 1:
        return 2
    if n % 2 == 0:
        return foo(n - 1) + foo(n - 2)
    else:
        return 1 + foo(n - 2)

# foo(bar(n)) growth: n^2

# 5
def bonk(n):
    sum = 0
    while n >= 2:
        sum += n
        n = n / 2
    return sum
# growth: lgN


#########################
# Extra
#########################
# 1. Write a function that creates a balanced binary search tree from a given sorted list. Its
# runtime should be in Θ(n), where n is the number of nodes in the tree. Binary search
# trees have an additional invariant (property) that each element in the right branch
# must be larger than the entry and each element in the left branch must be smaller
# than the entry.
def list_to_bst(lst):
    if len(lst) == 0:
        return BinaryTree.empty

    if len(lst) == 1:
        return BinaryTree(lst[0])

    mid = len(lst) // 2
    left = lst[:mid]
    right = lst[mid + 1:]

    return BinaryTree(lst[mid], list_to_bst(left), list_to_bst(right))


# 2. Give the running times of the functions g and h in terms of n. Then, for a bigger
# challenge, give the runtime of f in terms of x and y.
def f(x, y):
    if x == 0 or y == 0:
        return 1
    if x < y:
        return f(x, y-1)
    if x > y:
        return f(x-1, y)
    else:
        return f(x-1, y) + f(x, y-1)
# 1. if x is 0 or y is 0, then 1
# 2. if x is greater than y, then (x - y) + 2^y
# 3. if y is greater than x, then (y - x) + 2^x

def g(n):
    return f(n, n)
# 2^n

def h(n):
    return f(n, 1) 
# n





