;; 5.2 Extra Questions
;; 1. Write a Scheme function that when given an element, a list, and 
;;    a position, inserts the element into the list at the position.
(define (insert element lst position)
  (if (= position 0)
    (cons element lst)
    (cons (car lst)
          (insert element (cdr lst) (- position 1)))))

;; 2. Write a Scheme function that when given a list, such as (1 2 3 4), 
;; duplicates every element in the list (i.e. (1 1 2 2 3 3 4 4)).

(define (duplicate lst)
  (if (null? lst)
    '()
    (cons (car lst)
          (cons (car lst)
                (duplicate (cdr lst))))))


