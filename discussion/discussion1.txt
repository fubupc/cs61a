;1.3：

1. 65
2. output:
welcome to
cs61a
hello world

3. output:
h 2
u 2
h 2012
2014


;2.1:

1. foo(5) => error. int cannot be applied.
2. 0

;3.1:
1.
nom(4) => 4
om(-4) => 4
brian = nom(4) => 4
brian + 1 => error. add cannot be applied on int and None type.
michelle = om(-4) => 4
michelle + 1 => 5

2.
y = beep(x)
=> 6
; y is None
boop(x)
=> 7
y + beep(8)
=> error.


;4. environment diagrams.
1.
global frame |
    a: 1     |                                   
    b: ------|---> func b(b) [parent=Global]     
-------------|

b frame
    b: 1
    return: 2

b frame
    b: 2
    return: 4


2.
global frame
    add: ---------> func sub(a, b)
    sub: ---------> func min(x, y)

sub frame:
    x: 2
    y: 3
    return: 2

add frame:
    a: 2
    b: 2
    return: 0



