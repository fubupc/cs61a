# Question 1.2
def which_lecture(time, seats_left, is_lazy):
    if time < 1400 and seats_left > 0 and not is_lazy:
        print 'go to lecture'
    elif time > 1400 and seats_left > 0 and not is_lazy:
        print 'go to alt lecture'

    if is_lazy:
        print 'watch videos'


def which_lecture(time, seats_left, is_lazy):
    if seats_left > 0 and not is_lazy:
        if time < 1400:
            print 'go to lecture'
        else:
            print 'go to alt lecture'

    if is_lazy:
        print 'watch videos'


def which_lecture(time, seats_left, is_lazy):
    if is_lazy:
        print 'watch videos'
    else:
        if time < 1400 and seats_left > 0:
            print 'go to lecture'
        elif time > 1400 and seats_left > 0:
            print 'go to alt lecture'


# Question 1.4
def is_prime(n):
    i = 2
    while i < n:
        if (n % i) == 0:
            return False
        i += 1

    return True


def choose(n, k):
    """Returns the number of ways to choose K items from
    N items.
    >>> choose(5, 2)
    10
    >>> choose(20, 6)
    38760
    """
    def multiply(start, end):
        product = 1
        while start <= end:
            product = product * start
            start += 1
        return product

    return multiply(n - k + 1, n) / multiply(1, k)


# 2.1 Functions as argument values
def square(x):
    return x * x
def square_every_number(n):
    """Prints out the square of every integer from 1 to n.
    >>> square_every_number(3)
    1
    4
    9
    """
    i = 1
    while i <= n:
        print square(i)


def double(x):
    return 2 * x
def double_every_number(n):
    """Prints out the double of every integer from 1 to n.
    >>> double_every_number(3)
    2
    4
    6
    """
    i = 1
    while i <= n:
        print double(i)

def every(func, n):
    """Prints out all integers from 1 to n with func applied
    on them.
    >>> def square(x):
    ... return x * x
    >>> every(square, 3)
    1
    4
    9
    """
    i = 1
    while i <= n:
        print func(i)

def keep(cond, n):
    """Prints out all integers from 1 to n that return True
    when called with cond.
    >>> def is_even(x):
    ... # Even numbers have remainder 0 when divided by 2.
    ... return x % 2 == 0
    >>> keep(is_even, 5)
    2
    4
    """
    i = 1
    while i <= n:
        if cond(i):
            print i



# 2.4
def and_add(f, n):
    """Returns a new function. This new function takes an
    argument x and returns f(x) + n.
    >>> def square(x):
    ... return x * x
    >>> new_square = and_add(square, 3)
    >>> new_square(4) # 4 * 4 + 3
    19
    """
    def inner(x):
        return f(x) + n

    return inner


"""
>>> composed(square, two)(7)
4
>>> skipped(added(square, two))()(3)
11
>>> composed(two, square)(2)
2
"""


