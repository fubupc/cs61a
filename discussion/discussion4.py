from math import sqrt
def distance(city1, city2):
    lat_1, lon_1 = get_lat(city_1), get_lon(city_1)
    lat_2, lon_2 = get_lat(city_2), get_lon(city_2)
    return sqrt((lat_1 - lat_2)**2 + (lon_1 - lon_2)**2)


# 1.1
def closer_city(lat, lon, city1, city2):
    fake_city = make_city('Fake', lat, lon)
    if distance(city1, fake_city) < distance(city2, fake_city):
        return get_name(city1)
    else:
        return get_name(city2)


# 2.1.1
from math import pow
def rational_pow(x, e):
    return rational(pow(numer(x), e), pow(denom(x), e))

# 2.1.2
def factorial(n):
    return 1 if n == 0 else n * factorial(n - 1)

def approx_e(iter=100):
    result = rational(0, 1)
    i = 0
    while i < iter:
        result = add_rationals(result, rational(1, factorial(i)))
        i += 1

    return result


# 2.1.3
def inverse_rational(x):
    """Returns the inverse of the given non-zero rational
    number"""
    return rational(denom(x), numer(x))

def div_rationals(x, y):
    """Returns x / y for given rational x and non-zero
    rational y"""
    return mul_rationals(x, inverse_rational(y))


# 3.1.1
def make_unit(catchphrase, damage):
    return [catchphrase, damage]

def get_catchphrase(unit):
    return unit[0]

def get_damage(unit):
    return unit[1]


# 3.2.1
def battle(first, second):
    """Simulates a battle between the first and second unit
    >>> zealot = make_unit('My life for Aiur!', 16)
    >>> zergling = make_unit('GRAAHHH!', 5)
    >>> winner = battle(zergling, zealot)
    GRAAHHH!
    My life for Aiur!
    >>> winner is zealot
    True
    """
    print(get_catchphrase(first))
    print(get_catchphrase(second))

    if get_damage(first) > get_damage(second):
        return first
    else:
        return second


# 3.3.1
def make_resource_bundle(minerals, gas):
    def get(index):
        if index == 0:
            return minerals
        elif index == 1:
            return gas
        else:
            print('error')
    return get

def get_minerals(bundle):
    return bundle(0)

def get_gas(bundle):
    return bundle(1)


# 3.4.1
def make_building(unit, bundle):
    return [unit, bundle]

def get_unit(building):
    return building[0]

def get_bundle(building):
    return building[1]


# 3.3.2
def build_unit(building, bundle):
    """Constructs a unit if given the minimum amount of resources.
    Otherwise, prints an error message
    >>> barracks = make_building(make_unit('Go go go!', 6), make_resource_bundle(50, 0))
    >>> marine = build_unit(barracks, make_resource_bundle(20, 20))
    We require more minerals!
    >>> marine = build_unit(barracks, make_resource_bundle(50, 0))
    >>> print(get_catchphrase(marine))
    Go go go!
    """
    needed = get_bundle(building)

    if get_minerals(bundle) < get_minerals(needed):
        print('We require more minerals!')

    elif get_gas(bundle) < get_gas(needed):
        print('We require more vespene gas!')

    else:
        return get_unit(building)

# 3.4.3
def make_resource_bundle(minerals, gas):
    return [minerals, gas]

def get_minerals(bundle):
    return bundle[0]

def get_gas(bundle):
    return bundle[1]

