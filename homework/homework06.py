# CS 61A Fall 2014
# Name:
# Login:

class VendingMachine(object):
    """A vending machine that vends some product for some price.

    >>> v = VendingMachine('candy', 10)
    >>> v.vend()
    'Machine is out of stock.'
    >>> v.restock(2)
    'Current candy stock: 2'
    >>> v.vend()
    'You must deposit $10 more.'
    >>> v.deposit(7)
    'Current balance: $7'
    >>> v.vend()
    'You must deposit $3 more.'
    >>> v.deposit(5)
    'Current balance: $12'
    >>> v.vend()
    'Here is your candy and $2 change.'
    >>> v.deposit(10)
    'Current balance: $10'
    >>> v.vend()
    'Here is your candy.'
    >>> v.deposit(15)
    'Machine is out of stock. Here is your $15.'
    """
    def __init__(self, product, price):
        self.product = product
        self.price = price
        self.stock = 0
        self.balance = 0

    def restock(self, num):
        self.stock += num
        return 'Current %s stock: %s' % (self.product, self.stock)

    def vend(self):
        if self.stock == 0:
            return 'Machine is out of stock.'

        if self.balance < self.price:
            return 'You must deposit $%s more.' % (self.price - self.balance)

        change = self.balance - self.price

        self.stock -= 1
        self.balance = 0

        if change == 0:
            return 'Here is your %s.' % self.product
        else:
            return 'Here is your %s and $%s change.' % (self.product, change)

    def deposit(self, amount):
        if self.stock == 0:
            return 'Machine is out of stock. Here is your $%s.' % amount

        self.balance += amount
        return 'Current balance: $%s' % self.balance


class MissManners(object):
    """A container class that only forward messages that say please.

    >>> v = VendingMachine('teaspoon', 10)
    >>> v.restock(2)
    'Current teaspoon stock: 2'
    >>> m = MissManners(v)
    >>> m.ask('vend')
    'You must learn to say please first.'
    >>> m.ask('please vend')
    'You must deposit $10 more.'
    >>> m.ask('please deposit', 20)
    'Current balance: $20'
    >>> m.ask('now will you vend?')
    'You must learn to say please first.'
    >>> m.ask('please hand over a teaspoon')
    'Thanks for asking, but I know not how to hand over a teaspoon'
    >>> m.ask('please vend')
    'Here is your teaspoon and $10 change.'
    >>> really_fussy = MissManners(m)
    >>> really_fussy.ask('deposit', 10)
    'You must learn to say please first.'
    >>> really_fussy.ask('please deposit', 10)
    'Thanks for asking, but I know not how to deposit'
    >>> really_fussy.ask('please please deposit', 10)
    'Thanks for asking, but I know not how to please deposit'
    >>> really_fussy.ask('please ask', 'please deposit', 10)
    'Current balance: $10'
    >>> fussy_three = MissManners(3)
    >>> fussy_three.ask('add', 4)
    'You must learn to say please first.'
    >>> fussy_three.ask('please add', 4)
    'Thanks for asking, but I know not how to add'
    >>> fussy_three.ask('please __add__', 4)
    7
    """
    def __init__(self, obj):
        self.obj = obj

    def ask(self, message, *args):
        word = 'please '
        if message[:len(word)] != word:
            return 'You must learn to say please first.'

        command = message[len(word):]
        action = getattr(self.obj, command, False)

        if not action:
            return 'Thanks for asking, but I know not how to %s' % command

        return action(*args)





##########################################
#           Challenge Problem            #
# (You can delete this part if you want) #
##########################################

def make_instance(some_class):
    """Return a new object instance of some_class."""
    def get_value(name):
        if name in attributes:
            return attributes[name]
        else:
            value = some_class['get'](name)
            return bind_method(value, instance)

    def set_value(name, value):
        attributes[name] = value

    attributes = {}
    instance = {'get': get_value, 'set': set_value}
    return instance

def bind_method(value, instance):
    """Return value or a bound method if value is callable."""
    if callable(value):
        def method(*args):
            return value(instance, *args)
        return method
    else:
        return value

from functools import reduce

def make_class(attributes, base_classes=()):
    """Return a new class with attributes.

    attributes -- class attributes
    base_classes -- a sequence of classes
    """
    def mro():
        return [klass] + merge([c['mro']() for c in base_classes])


    def head(lst):
        return lst[0]

    def tail(lst):
        return lst[1:]

    def in_tail(item, lst):
        return item in reduce(lambda a, b: a + b, [tail(x) for x in lst], [])

    def remove_in_head(item, lst):
        return [x if head(x) != item else tail(x) for x in lst]

    def merge(mro_lst):
        mro_lst = [x for x in mro_lst if len(x) > 0]

        if len(mro_lst) == 0:
            return []

        for i in range(len(mro_lst)):
            current = mro_lst[i]
            before = mro_lst[:i]
            after = mro_lst[i + 1:]

            item = head(current)

            if not in_tail(item, before + after):
                before = remove_in_head(item, before) 
                after = remove_in_head(item, after) 
                return [item] + merge(before + [tail(current)] + after)

        raise Exception('mro cannot resolved.')

    def get_direct(name):
        if name in attributes:
            return attributes[name]

        return False

    def get(name):
        if get_direct(name):
            return get_direct(name)

        classes = mro()
        for c in classes:
            if c['get_direct'](name):
                return c['get_direct'](name)

        raise Exception('attribute %s not found.')


    def new(*args):
        return init_instance(klass, *args)

    klass = {'mro': mro, 'get': get, 'get_direct': get_direct, 'new': new}
    return klass


def init_instance(some_class, *args):
    """Return a new instance of some_class, initialized with args."""
    instance = make_instance(some_class)
    init = some_class['get']('__init__')
    if init:
        init(instance, *args)
    return instance

# AsSeenOnTVAccount example from lecture.

def make_account_class():
    """Return the Account class, which has deposit and withdraw methods."""

    interest = 0.02

    def __init__(self, account_holder):
        self['set']('holder', account_holder)
        self['set']('balance', 0)

    def deposit(self, amount):
        """Increase the account balance by amount and return the new balance."""
        new_balance = self['get']('balance') + amount
        self['set']('balance', new_balance)
        return self['get']('balance')

    def withdraw(self, amount):
        """Decrease the account balance by amount and return the new balance."""
        balance = self['get']('balance')
        if amount > balance:
            return 'Insufficient funds'
        self['set']('balance', balance - amount)
        return self['get']('balance')

    return make_class(locals())

Account = make_account_class()

def make_checking_account_class():
    """Return the CheckingAccount class, which imposes a $1 withdrawal fee.

    >>> checking = CheckingAccount['new']('Jack')
    >>> checking['get']('interest')
    0.01
    >>> checking['get']('deposit')(20)
    20
    >>> checking['get']('withdraw')(5)
    14
    """
    interest = 0.01
    withdraw_fee = 1

    def withdraw(self, amount):
        fee = self['get']('withdraw_fee')
        return Account['get']('withdraw')(self, amount + fee)

    return make_class(locals(), [Account])

CheckingAccount = make_checking_account_class()

def make_savings_account_class():
    """Return the SavingsAccount class, which imposes a $2 deposit fee.

    >>> savings = SavingsAccount['new']('Jack')
    >>> savings['get']('interest')
    0.02
    >>> savings['get']('deposit')(20)
    18
    >>> savings['get']('withdraw')(5)
    13
    """
    deposit_fee = 2

    def deposit(self, amount):
        fee = self['get']('deposit_fee')
        return Account['get']('deposit')(self, amount - fee)

    return make_class(locals(), [Account])

SavingsAccount = make_savings_account_class()

def make_as_seen_on_tv_account_class():
    """Return an account with lots of fees and a free dollar.

    >>> such_a_deal = AsSeenOnTVAccount['new']('Jack')
    >>> such_a_deal['get']('balance')
    1
    >>> such_a_deal['get']('interest')
    0.01
    >>> such_a_deal['get']('deposit')(20)
    19
    >>> such_a_deal['get']('withdraw')(5)
    13
    """
    def __init__(self, account_holder):
        self['set']('holder', account_holder)
        self['set']('balance', 1)

    return make_class(locals(), [CheckingAccount, SavingsAccount])

AsSeenOnTVAccount = make_as_seen_on_tv_account_class()
