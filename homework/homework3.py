# CS 61A Fall 2014
# Name:
# Login:

def g(n):
    """Return the value of G(n), computed recursively.

    >>> g(1)
    1
    >>> g(2)
    2
    >>> g(3)
    3
    >>> g(4)
    10
    >>> g(5)
    22
    """
    if n <= 3:
        return n
    else:
        return g(n - 1) + 2 * g(n - 2) + 3 * g(n - 3)

def g_iter(n):
    """Return the value of G(n), computed iteratively.

    >>> g_iter(1)
    1
    >>> g_iter(2)
    2
    >>> g_iter(3)
    3
    >>> g_iter(4)
    10
    >>> g_iter(5)
    22
    """
    if n <= 3:
        return n
    else:
        prev2, prev1, curr = 1, 2, 3
        count = n - 3
        while count > 0:
            prev2, prev1, curr = prev1, curr, (curr + 2 * prev1 + 3 * prev2)
            count -= 1
        return curr

def has_seven(k):
    """Returns True if at least one of the digits of k is a 7, False otherwise.

    >>> has_seven(3)
    False
    >>> has_seven(7)
    True
    >>> has_seven(2734)
    True
    >>> has_seven(2634)
    False
    >>> has_seven(734)
    True
    >>> has_seven(7777)
    True
    """
    if k < 10:
        return (k % 10) == 7
    else:
        return (k % 10) == 7 or has_seven(k // 10)

def pingpong(n):
    """Return the nth element of the ping-pong sequence.

    >>> pingpong(7)
    7
    >>> pingpong(8)
    6
    >>> pingpong(15)
    1
    >>> pingpong(21)
    -1
    >>> pingpong(22)
    0
    >>> pingpong(30)
    6
    >>> pingpong(68)
    2
    >>> pingpong(69)
    1
    >>> pingpong(70)
    0
    >>> pingpong(71)
    1
    >>> pingpong(72)
    0
    >>> pingpong(100)
    2
    """

    def add_one(v):
        return v + 1

    def minus_one(v):
        return v - 1

    def reverse(op):
        if op is add_one:
            return minus_one
        else:
            return add_one

    # iterative
    """
    k, val, op = 1, 1, add_one
    while k <= n:
        k, val = k + 1, op(val)

        if (k % 7 == 0) or has_seven(k):
            op = reverse(op)
    return val
    """

    # recursive
    def helper(k, val, op):
        if k == n:
            return val
        if (k % 7 == 0) or has_seven(k):
            return helper(k + 1, reverse(op)(val), reverse(op))
        else:
            return helper(k + 1, op(val), op)

    return helper(1, 1, add_one)


def count_change(amount):
    """Return the number of ways to make change for amount.

    >>> count_change(7)
    6
    >>> count_change(10)
    14
    >>> count_change(20)
    60
    >>> count_change(100)
    9828
    """
    def change_helper(min_coin, amount):
        if amount == min_coin:
            return 1
        if amount < min_coin:
            return 0
        return change_helper(min_coin * 2, amount) + change_helper(min_coin, amount - min_coin) 

    return change_helper(1, amount)

def towers_of_hanoi(n, start, end):
    """Print the moves required to solve the towers of hanoi game, starting
    with n disks on the start pole and finishing on the end pole.

    The game is to assumed to have 3 poles.

    >>> towers_of_hanoi(1, 1, 3)
    Move the top disk from rod 1 to rod 3
    >>> towers_of_hanoi(2, 1, 3)
    Move the top disk from rod 1 to rod 2
    Move the top disk from rod 1 to rod 3
    Move the top disk from rod 2 to rod 3
    >>> towers_of_hanoi(3, 1, 3)
    Move the top disk from rod 1 to rod 3
    Move the top disk from rod 1 to rod 2
    Move the top disk from rod 3 to rod 2
    Move the top disk from rod 1 to rod 3
    Move the top disk from rod 2 to rod 1
    Move the top disk from rod 2 to rod 3
    Move the top disk from rod 1 to rod 3
    """
    assert 0 < start <= 3 and 0 < end <= 3 and start != end, "Bad start/end"

    def move_one(start, end):
        print("Move the top disk from rod %s to rod %s" % (start, end))

    third = 6 - start - end

    if n == 1:
        move_one(start, end)
    else:
        towers_of_hanoi(n - 1, start, third)
        move_one(start, end)
        towers_of_hanoi(n - 1, third, end)

from operator import sub, mul

def make_anonymous_factorial():
    """Return the value of an expression that computes factorial.

    >>> make_anonymous_factorial()(5)
    120
    """
    # reference: https://en.wikipedia.org/wiki/Lambda_calculus (section: recursion and fixed point)
    """
    def g(r):
        def f(n):
            if n == 0:
                return 1
            else:
                return n * r(r)(n - 1)
        return f

    return g(g)
    """
    return (lambda r: lambda n: 1 if n == 0 else n * r(r)(n - 1))(lambda r: lambda n: 1 if n == 0 else n * r(r)(n - 1))

