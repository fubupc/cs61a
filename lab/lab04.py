# helper functions
apply_to_all = map
keep_if = filter
from functools import reduce

# Question 1
"""
>>> x = [1, 2, 3]
>>> x[0]
1
>>> x[3]
IndexError
>>> x[-1]
3
>>> x[-3]
1
"""


# Question 2
"""
>>> x = [1, 2, 3, 4]
>>> x[1:3]
[2, 3]
>>> x[:2]
[1, 2]
>>> x[1:]
[2, 3, 4]
>>> x[-2:3]
[3]
>>> x[::2]
[1, 3]
>>> x[::-1]
[4, 3, 2, 1]
>>> x[-1:0:-1]
[4, 3, 2]
"""

# Question 3
"""
>>> y = [1]
>>> len(y)
1
>>> 1 in y
True
>>> y + [2, 3]
[1, 2, 3]
>>> [0] + y
[0, 1]
>>> y * 3
[1, 1, 1]
>>> z = [[1, 2], [3, 4, 5]]
>>> len(z)
2
"""

# Question 4
def get_seven_a(x):
    """
    >>> x = [1, 3, [5, 7], 9]
    >>> get_seven_a(x)
    7
    """
    return x[2][1]

def get_seven_b(x):
    """
    >>> x = [[7]]
    >>> get_seven_b(x)
    7
    """
    return x[0][0]

def get_seven_c(x):
    """
    >>> x = [1, [2, [3, [4, [5, [6, [7]]]]]]]
    >>> get_seven_c(x)
    7
    """
    return x[1][1][1][1][1][1][0]


# Question 5
def reverse_iter(lst):
    """Returns the reverse of the given list.

    >>> reverse_iter([1, 2, 3, 4])
    [4, 3, 2, 1]
    """
    result = []
    for i in lst:
        result = [i] + result
    return result

def reverse_recursive(lst):
    """Returns the reverse of the given list.

    >>> reverse_recursive([1, 2, 3, 4])
    [4, 3, 2, 1]
    """
    if len(lst) == 0:
        return []
    else:
        return reverse_recursive(lst[1:]) + lst[0:1]


# Question 6
def deep_len(lst):
    """Returns the deep length of the list.

    >>> deep_len([1, 2, 3])     # normal list
    3
    >>> x = [1, [2, 3], 4]      # deep list
    >>> deep_len(x)
    4
    >>> x = [[1, [1, 1]], 1, [1, 1]] # deep list
    >>> deep_len(x)
    6
    """
    if type(lst) != list:
        return 1
    elif len(lst) == 0:
        return 0
    else:
        return deep_len(lst[0]) + deep_len(lst[1:])

# Question 7
def merge(lst1, lst2):
    """Merges two sorted lists recursively.

    >>> merge([1, 3, 5], [2, 4, 6])
    [1, 2, 3, 4, 5, 6]
    >>> merge([], [2, 4, 6])
    [2, 4, 6]
    >>> merge([1, 2, 3], [])
    [1, 2, 3]
    >>> merge([5, 7], [2, 4, 6])
    [2, 4, 5, 6, 7]
    """
    # recursive
    if len(lst1) == 0:
        return lst2
    elif len(lst2) == 0:
        return lst1
    elif lst1[0] <= lst2[0]:
        return [lst1[0]] + merge(lst1[1:], lst2)
    else:
        return [lst2[0]] + merge(lst1, lst2[1:])

# iterative
def merge_iter(lst1, lst2):
    idx1, idx2 = 0, 0
    result = []

    while idx1 < len(lst1) and idx2 < len(lst2):
        if lst1[idx1] <= lst2[idx2]:
            result += [lst1[idx1]]
            idx1 += 1
        else:
            result += [lst2[idx2]]
            idx2 += 1

    return result + lst1[idx1:] + lst2[idx2:]


# Question 8
def mergesort(seq):
    """Mergesort algorithm.

    >>> mergesort([4, 2, 5, 2, 1])
    [1, 2, 2, 4, 5]
    >>> mergesort([])     # sorting an empty list
    []
    >>> mergesort([1])   # sorting a one-element list
    [1]
    """
    if len(seq) <= 1:
        return seq
    else:
        mid = len(seq) // 2
        return merge(mergesort(seq[:mid]), mergesort(seq[mid:]))

# iterative
def mergesort_iter(seq):
    result = [[x] for x in seq]
    while len(result) > 1:
        if len(result) % 2 == 0:
            result = [merge(result[i], result[i + 1]) for i in range(len(result) // 2)]
        else:
            result = [merge(result[i], result[i + 1]) for i in range(len(result) // 2)] + result[-1:]

    return result[0]

# better solution
def mergesort_iter(seq):
    if not seq:
        return []

    queue = [[elem] for elem in seq]
    while len(queue) > 1:
        first, second = queue[0], queue[1]
        queue = queue[2:] + [merge(first, second)]

    return queue[0]


# Question 9
"""
>>> apply_to_all(lambda x: x**2, [1, 2, 3, 4])
[1, 4, 9, 16]
>>> apply_to_all(lambda a: a, 'cs61a')
['c', 's', '6', '1', 'a']
>>> keep_if(lambda x: x % 2 == 0, [1, 2, 3, 4])
[2, 4]
>>> reduce(lambda a, b: a + b, [1, 2, 3, 4, 5], 0)
15
>>> reduce(lambda a, b: b + a, 'hello world!', '')
'hello world!'
"""

# Question 10
"""
>>> apply_to_all(lambda x: 1 if x > 0 else -1, [1, 3, -1, -4, 2])
[1, 1, -1, -1, 1]
>>> keep_if(lambda x: (x == 1) or (x % 2 == 0), [1, 7, 14, 21, 28, 35, 42])
[1, 14, 28, 42]
>>> reduce(lambda accum, item: item + accum, 'hello', '')
'olleh'
>>> reduce(lambda accum, w: accum + w, apply_to_all(lambda c: c + 'a', 'nnnnn'), '') + ' batman!'
'nanananana batman!'
"""


# Question 11
def coords(fn, seq, lower, upper):
    """
    >>> seq = [-4, -2, 0, 1, 3]
    >>> fn = lambda x: x**2
    >>> coords(fn, seq, 1, 9)
    [[-2, 4], [1, 1], [3, 9]]
    """ 
    return [[x, fn(x)] for x in seq if lower <= fn(x) <= upper]


# Question 12
def add_matrices(x, y):
    """
    >>> add_matrices([[1, 3], [2, 0]], [[-3, 0], [1, 2]])
    [[-2, 3], [3, 2]]
    """
    # solution 1: nested map
    # return list(map(lambda m, n: list(map(lambda a, b: a + b, m, n)), x, y))

    # solution 2: list comprehension
    return [[x[i][j] + y[i][j] for j in range(len(x[i]))] for i in range(len(x))]


# Question 13
# Now write a list comprehension that will create a deck of cards. Each element 
# in the list will be a card, which is represented by a list containing the suit 
# as a string and the value as an int.  
def deck():
    suits = ["spades", "clubs", "diamonds", "hearts"]
    values = [i for i in range(1, 14)]

    # return reduce(lambda x, y: x + y, [[[s, v] for v in values ] for s in suits], [])

    # better
    return [[s, v] for s in suits for v in values]


def sort_deck(deck):
    # better. utilize stable property of sorted
    deck = sorted(deck, key=lambda card: card[1])
    return sorted(deck, key=lambda card: card[0])

