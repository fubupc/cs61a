
#################################
# basic code
#################################
class Link:
    """A linked list.

    >>> s = Link(1, Link(2, Link(3, Link(4))))
    >>> len(s)
    4
    >>> s[2]
    3
    >>> s
    Link(1, Link(2, Link(3, Link(4))))
    """
    empty = ()

    def __init__(self, first, rest=empty):
        self.first = first
        self.rest = rest

    def __getitem__(self, i):
        if i == 0:
            return self.first
        else:
            return self.rest[i-1]

    def __len__(self):
        return 1 + len(self.rest)

    def __repr__(self):
        if self.rest:
            rest_str = ', ' + repr(self.rest)
        else:
            rest_str = ''
        return 'Link({0}{1})'.format(repr(self.first), rest_str)


# Question 1
def Q1():
    """
    >>> Link()
    Error. Argument number less than required.
    >>> link = Link(1, Link(2, Link(3)))
    >>> link.first
    1
    >>> link.rest.first
    2
    >>> link.rest.rest.rest is Link.empty
    True
    >>> link.first = 9001
    >>> link.first
    9001
    >>> link.rest = link.rest.rest
    >>> link.rest.first
    3
    >>> link = Link(1)
    >>> link.rest = link
    >>> link.rest.rest.rest.rest.first
    1
    """
    pass


# Question 2
def insert(link, value, index):
    """Insert a value into a Link at the given index.

    >>> link = Link(1, Link(2, Link(3)))
    >>> insert(link, 9001, 0)
    >>> link
    Link(9001, Link(1, Link(2, Link(3))))
    >>> insert(link, 100, 2)
    >>> link
    Link(9001, Link(1, Link(100, Link(2, Link(3)))))
    >>> insert(link, 4, 5)
    Index out of bounds
    """
    if link is Link.empty:
        print('Index out of bounds')
        return

    if index == 0:
        link.rest = Link(link.first, link.rest)
        link.first = value
    else:
        insert(link.rest, value, index - 1)


# Question 3
class Link:
    """A linked list.

    >>> s = Link(1, Link(2, Link(3, Link(4))))
    >>> len(s)
    4
    >>> s[2]
    3
    >>> s
    Link(1, Link(2, Link(3, Link(4))))
    """
    empty = ()

    def __init__(self, first, rest=empty):
        self.first = first
        self.rest = rest

    def __getitem__(self, i):
        if i == 0:
            return self.first
        else:
            return self.rest[i-1]

    def __len__(self):
        return 1 + len(self.rest)

    def __repr__(self):
        if self.rest:
            rest_str = ', ' + repr(self.rest)
        else:
            rest_str = ''
        return 'Link({0}{1})'.format(repr(self.first), rest_str)

    def __str__(self):
        """Returns a human-readable string representation of the Link

        >>> s = Link(1, Link(2, Link(3, Link(4))))
        >>> str(s)
        '<1, 2, 3, 4>'
        >>> str(Link(1))
        '<1>'
        >>> str(Link.empty)  # empty tuple
        '()'
        """

        # iterative #2
        res = '<'
        current = self
        while current.rest is not Link.empty:
            res += str(current.first) + ', '
            current = current.rest
        return res + str(current.first) + '>'

        # iterative #1
        res = '<' + str(self.first)
        for x in self.rest:
            res += ', ' + str(x)
        return res + '>'

        # recursive
        if self.rest is Link.empty:
            return '<' + str(self.first) + '>'
        else:
            return '<' + str(self.first) + ', ' + str(self.rest)[1:]

