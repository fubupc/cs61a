# coding=utf-8

# Question 1
True
False
True
False

# Question 2
"""
>>> True and 1 / 0 == 1 and False
ZeroDivisionError

>>> True or 1 / 0 == or False
True

>>> True and 0
0

>>> False or 1
1

>>> 1 and 3 and 6 and 10 and 15
1

>>> "" or 0 or False or 2 or 1 / 0
2
"""


# Question 3
def both_positive(x, y):
    """
    Returns True if both x and y are positive.
    >>> both_positive(-1, 1)
    False
    >>> both_positive(1, 1)
    True
    """
    "*** YOUR CODE HERE ***"
    return x > 0 and y > 0

# Question 4
def gets_discount(x, y):
    """ Returns True if this is a combination of a senior citizen
    and a child, False otherwise.

    >>> gets_discount(65, 12)
    True
    >>> gets_discount(9, 70)
    True
    >>> gets_discount(40, 45)
    False
    >>> gets_discount(40, 75)
    False
    >>> gets_discount(65, 13)
    False
    >>> gets_discount(7, 9)
    False
    >>> gets_discount(73, 77)
    False
    >>> gets_discount(70, 31)
    False
    >>> gets_discount(10, 25)
    False
    """
    return (x >= 65 and y <= 12) or (x <= 12 and y >= 65)


# Question 5
def is_factor(x, y):
    """ Returns True if x is a factor of y, False otherwise.

    >>> is_factor(3, 6)
    True
    >>> is_factor(4, 10)
    False
    >>> is_factor(0, 5)
    False
    >>> is_factor(0, 0)
    False
    """
    return (x != 0) and (y % x == 0)


# Question 6
"""
>>> a, b = 10, 6
>>> if a == 4:
...     6
... elif b >= 4:
...     6 + 7 + a
... else: 
...     25
23
"""


#Question 7: Fix the Bug II
"""The following snippet of code doesn't work! Figure out what is wrong and fix the bugs."""

def compare(a, b):
    """ Compares if a and b are equal.

    >>> compare(4, 2)
    'not equal'
    >>> compare(4, 4)
    'equal'
    """
    if a == b:
        return 'equal'
    return 'not equal'


#Question 8: What Would Python Print?
"""
>>> n = 3
>>> while n >= 0:
...     n -= 1
...     print(n)
...
2
1
0
-1

>>> n, i = 7, 0
>>> while i < n:
...     i += 2
...     print(i)
...
2
4
6
8

>>> # typing Ctrl-C will stop infinite loops
>>> n = 4
>>> while True:
...     n -= 1
...     print(n)
...
3
2
1
0
-1

... # continues forever
KeyboardInterrupt

>>> n = 2
>>> def exp_decay(n):
...     if n % 2 != 0:
...         return
...     while n > 0:
...         print(n)
...         n = n // 2
...
>>> exp_decay(64)
64
32
16
8
4
2
1

>>> exp_decay(5)
# No output
"""


# Question 9: Factorials
"""Let's write a function falling, which is a "falling" factorial that takes two arguments, n and
k, and returns the product of k consecutive numbers, starting from n and working downwards."""

def falling(n, k):
    """
    Compute the falling factorial of n to depth k.

    >>> falling(6, 3)  # 6 * 5 * 4
    120
    >>> falling(4, 0)
    1
    """
    factorial = 1
    while k >= 0:
        factorial = factorial * n
        k -= 1
        n -= 1
    return factorial


# Question 10: Factor This II*

"""Define a function factors(n) which takes in a number, n, and prints out all of the numbers that divide n evenly. For example, a call with n = 20 should result as follows:  """

def factors(n):
    """
    Prints out all of the numbers that divide `n` evenly.

    >>> factors(20)
    20
    10
    5
    4
    2
    1
    """
    "*** YOUR CODE HERE ***"


# Question 11: What Would Python Output?
"""
>>> def square(x):
...     return x*x
...
>>> def neg(f, x):
...     return -f(x)
...
>>> neg(square, 4)
-16

>>> def even(f):
...     def odd(x):
...         if x < 0:
...             return f(-x)
...         return f(x)
...     return odd
...
>>> def identity(x):
...     return x
...
>>> triangle = even(identity)
>>> triangle(61)
61

>>> triangle(-4)
4

>>> def first(x):
...     x += 8
...     def second(y):
...         print('second')
...         return x + y
...     print('first')
...     return second
...
>>> f = first(15)
first

>>> f(16)
second
39
"""

# Question 12: Flight of the Bumblebee

"""Write a function that takes in a number n and returns a function that takes in a number range 
which will print all numbers from 0 to range (including 0 but excluding range) but print Buzz! 
instead for all the numbers that are divisible by n."""

def make_buzzer(n):
    """ Returns a function that prints numbers in a specified
    range except those divisible by n.

    >>> i_hate_fives = make_buzzer(5)
    >>> i_hate_fives(10)
    Buzz!
    1
    2
    3
    4
    Buzz!
    6
    7
    8
    9
    """
    def hater(r):
        i = 0
        while i < r:
            if (i % n) == 0:
                print "Buzz!"
            else:
                print i
            i += 1
    return hater


# Question 13: I Heard You Liked Functions...*

"""This question is extremely challenging. Use it to test if you have really mastered the material!

Define a function cycle that takes in three functions f1, f2, f3, as arguments. cycle will return another function that should take in an integer argument n and return another function. That final function should take in an argument x and cycle through applying f1, f2, and f3 to x, depending on what n was. Here’s the what the final function should do to x for a few values of n:

n = 0, return x
n = 1, apply f1 to x, or return f1(x)
n = 2, apply f1 to x and then f2 to the result of that, or return f2(f1(x))
n = 3, apply f1 to x, f2 to the result of applying f1, and then f3 to the result of applying f2, or f3(f2(f1(x)))
n = 4, start the cycle again applying f1, then f2, then f3, then f1 again, or f1(f3(f2(f1(x))))
And so forth.
Hint: most of the work goes inside the most nested function.
"""

def cycle(f1, f2, f3):
    """ Returns a function that is itself a higher order function
    >>> def add1(x):
    ...     return x + 1
    ...
    >>> def times2(x):
    ...     return x * 2
    ...
    >>> def add3(x):
    ...     return x + 3
    ...
    >>> my_cycle = cycle(add1, times2, add3)
    >>> identity = my_cycle(0)
    >>> identity(5)
    5
    >>> add_one_then_double = my_cycle(2)
    >>> add_one_then_double(1)
    4
    >>> do_all_functions = my_cycle(3)
    >>> do_all_functions(2)
    9
    >>> do_more_than_a_cycle = my_cycle(4)
    >>> do_more_than_a_cycle(2)
    10
    >>> do_two_cycles = my_cycle(6)
    >>> do_two_cycles(1)
    19
    """

    fn_list = (f1, f2, f3)

    def make_cycle(n):
        def do(x):
            if n == 0:
                return x

            result = x
            i = 0
            while i < n:
                result = fn_list[i % 3](result)
                i += 1
            return result

        return do

    return make_cycle




