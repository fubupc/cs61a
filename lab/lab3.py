# Question 1: What would Python print?
"""
>>> a = lambda: 5
>>> a()
5

>>> a(5)
Error.

>>> a()()
ERROR.

>>> b = lambda: lambda x: 3
>>> b()(15)
3

>>> c = lambda x, y: x + y
>>> c(4, 5)
9

>>> d = lambda x: c(a(), b()(x))
>>> d(2)
8

>>> b = lambda: lambda x: x
>>> d(2)
7

>>> e = lambda x: lambda y: x * y
>>> e(3)
<function <lambda> 0xXXXXXXXXXXXXXXX>

>>> e(3)(3)
9

>>> f = e(2)
>>> f(5)
10

>>> f(6)
12

>>> g = lambda: print(1) # When is the body of this function run?
# Nothing is printed

>>> h = g()
1

>>> print(h)
None
"""


# Question 2
"""
>>> # Part 1
>>> a = lambda x : x * 2 + 1
>>> def b(x):
...     return x * y
...
>>> y = 3
>>> b(y)
9

>>> def c(x):
...     y = a(x)
...     return b(x) + a(x+y)
...
>>> c(y)
30

>>> # Part 2: This one is pretty tough. A carefully drawn environment
>>> # diagram will be really useful.
>>> g = lambda x: x + 3
>>> def wow(f):
...     def boom(g):
...       return f(g)
...     return boom
...
>>> f = wow(g)
>>> f(2)
5

>>> g = lambda x: x * x
>>> f(3)
6
"""

# Question 3
def f1():
    """
    >>> f1()
    3
    """
    return 3

def f2():
    """
    >>> f2()()
    3
    """
    return lambda: 3

def f3():
    """
    >>> f3()(3)
    3
    """
    return lambda x: x

def f4():
    """
    >>> f4()()(3)()
    3
    """
    return lambda: lambda x: lambda: x


# Question 4

def lambda_curry2(func):
    """
    Returns a Curried version of a two argument function func.
    >>> from operator import add
    >>> x = lambda_curry2(add)
    >>> y = x(3)
    >>> y(5)
    8
    """
    "*** YOUR CODE HERE ***"
    return lambda x: lambda y: func(x, y)


# Question 6: In sum...
def sum(n):
    """Computes the sum of all integers between 1 and n, inclusive.
    Assume n is positive.

    >>> sum(1)
    1
    >>> sum(5)  # 1 + 2 + 3 + 4 + 5
    15
    """
    if n == 1:
        return 1
    else:
        return n + sum(n - 1)

#Question 7: Misconceptions

#The following examples of recursive functions show some examples of common recursion mistakes. Fix them so that they work as intended.

def sum_every_other_number(n):
    """Return the sum of every other natural number 
    up to n, inclusive.

    >>> sum_every_other_number(8)
    20
    >>> sum_every_other_number(9)
    25
    """
    if n == 0:
        return 0
    elif n == 1:
        return 1
    else:
        return n + sum_every_other_number(n - 2)

def fibonacci(n):
    """Return the nth fibonacci number.

    >>> fibonacci(11)
    89
    """
    if n == 0:
        return 0
    elif n == 1:
        return 1
    else:
        return fibonacci(n - 1) + fibonacci(n - 2)

#This one will take a while. You can return to this after you complete the other questions in lab:
def even_digits(n):
    """Return the percentage of digits in n that are even.

    >>> even_digits(23479837) # 3 / 8
    0.375
    """
    def _inner(n, num_digits, num_evens):
        if n == 0:
            return num_evens / num_digits

        least = n % 10
        remain = n // 10

        if least % 2 == 0:
            return _inner(remain, num_digits + 1, num_evens + 1)
        else:
            return _inner(remain, num_digits + 1, num_evens)

    return _inner(n, 0, 0)


# Question 8: Hailstone

#For the hailstone function from homework 1, you pick a positive integer n as the start. If n is even, divide it by 2. If n is odd, multiply it by 3 and add 1. Repeat this process until n is 1. Write a recursive version of hailstone that prints out the values of the sequence and returns the number of steps.

def hailstone(n):
    """Print out the hailstone sequence starting at n, and return the
    number of elements in the sequence.

    >>> a = hailstone(10)
    10
    5
    16
    8
    4
    2
    1
    >>> a
    7
    """
    # recursive function. but iterative by nature.
    def helper(n, count):
        print(n)
        count = count + 1
        if n == 1:
            return count
        elif n % 2 == 0:
            return helper(n // 2, count)
        else:
            return helper(n * 3 + 1, count)

    # real recursive.
    def helper2(n):
        print(n)
        if n == 1:
            return 1
        elif n % 2 == 0:
            return 1 + helper2(n // 2)
        else:
            return 1 + helper2(n * 3 + 1)

    return helper2(n)


# Question 9: Insect Combinatorics*

#Consider an insect in an M by N grid. The insect starts at the bottom left corner, (0, 0), and wants to end up at the top right corner, (M-1, N-1). The insect is only capable of moving right or up. Write a function paths that takes a grid length and width and returns the number of different paths the insect can take from the start to the goal. (There is a closed-form solution to this problem, but try to answer it procedurally using recursion.)

# For example, the 2 by 2 grid has a total of two ways for the insect to move from the start to the goal. For the 3 by 3 grid, the insect has 6 diferent paths (only 3 are shown above).

def paths(m, n):
    """Return the number of paths from one corner of an
    M by N grid to the opposite corner.

    >>> paths(2, 2)
    2
    >>> paths(5, 7)
    210
    >>> paths(117, 1)
    1
    >>> paths(1, 157)
    1
    """
    if n == 1:
        return 1
    else:
        total, i = 0, 0
        while i < m:
            total += paths(m - i, n - 1)
            i += 1
        return total

# better solution:
def paths(m, n):
    """Return the number of paths from one corner of an
    M by N grid to the opposite corner.

    >>> paths(2, 2)
    2
    >>> paths(5, 7)
    210
    >>> paths(117, 1)
    1
    >>> paths(1, 157)
    1
    """
    if m == 1 or n == 1:
        return 1
    return paths(m - 1, n) + paths(m, n - 1)


#Question 10: GCD*

#The greatest common divisor of two positive integers a and b is the largest integer which evenly divides both numbers (with no remainder). Euclid, a Greek mathematician in 300 B.C., realized that the greatest common divisor of a and b is one of the following:

#the smaller value if it evenly divides the larger value, OR
#the greatest common divisor of the smaller value and the remainder of the larger value divided by the smaller value
#In other words, if a is greater than b and a is not divisible by b, then

#gcd(a, b) == gcd(b, a % b)
#Write the gcd function recursively using Euclid's algorithm.

def gcd(a, b):
    """Returns the greatest common divisor of a and b.
    Should be implemented using recursion.

    >>> gcd(34, 19)
    1
    >>> gcd(39, 91)
    13
    >>> gcd(20, 30)
    10
    >>> gcd(40, 40)
    40
    """
    def helper(larger, smaller):
        if larger % smaller == 0:
            return smaller
        else:
            return helper(smaller, larger % smaller)

    if a > b:
        return helper(a, b)
    else:
        return helper(b, a)


