; Don't edit! Used for tests!
(define nil '())
(define (assert-equal test-num func-name actual-result expected-result)
  (if (not (equal? expected-result actual-result))
      (begin
        (display "Testing case ")
        (display test-num)
        (display (string-append " for " func-name ": Test failed. "))
        (display "Expected: ")
        (display expected-result)
        (display " Got: ")
        (display actual-result)
        (newline))
      (begin (display "Ok") (newline))))


;; Question 5
(define (gcd a b)
  (cond ((< a b) (gcd b a))
        ((= b 0) a)
        (else (gcd b (remainder a b)))))

; Tests for gcd
(if (not (equal? (gcd 0 0) 'your-code-here))
    (begin
      (assert-equal 1 "gcd" (gcd 0 4) 4)
      (assert-equal 2 "gcd" (gcd 8 0) 8)
      (assert-equal 3 "gcd" (gcd 34 19) 1)
      (assert-equal 4 "gcd" (gcd 39 91) 13)
      (assert-equal 5 "gcd" (gcd 20 30) 10)
      (assert-equal 6 "gcd" (gcd 40 40) 40))
    (display "gcd not implemented!"))

;; Question 9
(define (remove item lst)
  (cond ((null? lst) '())
        ((equal? item (car lst)) (remove item (cdr lst)))
        (else (cons (car lst) (remove item (cdr lst))))))


;; Question 10
(define (filter f lst)
  (if (null? lst)
    '()
    (if (f (car lst))
      (cons (car lst) (filter f (cdr lst)))
      (filter f (cdr lst)))))


;; Question 11
(define (all-satisfies lst pred)
  (cond ((null? lst) true)
        ((pred (car lst)) (all-satisfies (cdr lst) pred))
        (else false)))


;; Question 13
(define (make-btree entry left right)
  (cons entry (cons left right)))

(define test-tree
  (make-btree 2
              (make-btree 1
                          nil
                          nil)
              (make-btree 4
                          (make-btree 3
                                      nil
                                      nil)
                          nil)))
; Represents:
;     2
;    / \
;   1   4
;      /
;     3

(define (entry tree)
  (car tree))

(define (left tree)
  (car (cdr tree)))

(define (right tree)
  (cdr (cdr tree)))

(entry test-tree)
(entry (left test-tree))
(entry (right test-tree))

(define (num-leaves tree)
  (cond ((null? tree) 0)
        ((and (null? (left tree))
              (null? (right tree)))
         1)
        (else (+ (num-leaves (left tree))
                 (num-leaves (right tree))))))

(if (not (equal? (num-leaves nil) 'your-code-here))
    (begin
      (assert-equal 1 "num-leaves" (num-leaves test-tree) 2)
      (assert-equal 2 "num-leaves" (num-leaves (right test-tree)) 1)
      (assert-equal 3 "num-leaves" (num-leaves nil) 0))
    (display "num-leaves not implemented!"))

