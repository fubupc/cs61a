"""The Game of Hog."""

from dice import four_sided, six_sided, make_test_dice
from ucb import main, trace, log_current_line, interact

GOAL_SCORE = 100 # The goal of Hog is to score 100 points.

######################
# Phase 1: Simulator #
######################

def roll_dice(num_rolls, dice=six_sided):
    """Roll DICE for NUM_ROLLS times.  Return either the sum of the outcomes,
    or 1 if a 1 is rolled (Pig out). This calls DICE exactly NUM_ROLLS times.

    num_rolls:  The number of dice rolls that will be made; at least 1.
    dice:       A zero-argument function that returns an integer outcome.
    """
    # These assert statements ensure that num_rolls is a positive integer.
    assert type(num_rolls) == int, 'num_rolls must be an integer.'
    assert num_rolls > 0, 'Must roll at least once.'
    score = 0
    one_rolled = False
    while num_rolls > 0:
        point = dice()
        if one_rolled:
            score = 1  # Don't exit here for GUI and test to work.
        elif point == 1:
            one_rolled = True
            score = 1
        else:
            score += point
        num_rolls -= 1

    return score


def take_turn(num_rolls, opponent_score, dice=six_sided):
    """Simulate a turn rolling NUM_ROLLS dice, which may be 0 (Free bacon).

    num_rolls:       The number of dice rolls that will be made.
    opponent_score:  The total score of the opponent.
    dice:            A function of no args that returns an integer outcome.
    """
    assert type(num_rolls) == int, 'num_rolls must be an integer.'
    assert num_rolls >= 0, 'Cannot roll a negative number of dice.'
    assert num_rolls <= 10, 'Cannot roll more than 10 dice.'
    assert opponent_score < 100, 'The game should be over.'
    if num_rolls == 0:
        return 1 + abs((opponent_score // 10) - (opponent_score % 10))
    else:
        return roll_dice(num_rolls, dice)


def select_dice(score, opponent_score):
    """Select six-sided dice unless the sum of SCORE and OPPONENT_SCORE is a
    multiple of 7, in which case select four-sided dice (Hog wild).
    """
    if (score + opponent_score) % 7 == 0:
        return four_sided
    else:
        return six_sided

def bid_for_start(bid0, bid1, goal=GOAL_SCORE):
    """Given the bids BID0 and BID1 of each player, returns three values:

    - the starting score of player 0
    - the starting score of player 1
    - the number of the player who rolls first (0 or 1)
    """
    assert bid0 >= 0 and bid1 >= 0, "Bids should be non-negative!"
    assert type(bid0) == int and type(bid1) == int, "Bids should be integers!"

    # The buggy code is below:
    if bid0 == bid1:
        return goal, goal, 0
    if bid0 == bid1 - 5:
        return 0, 10, 1
    if bid1 == bid0 - 5:
        return 10, 0, 0
    if bid1 > bid0:
        return bid1, bid0, 1
    else:
        return bid1, bid0, 0

def other(who):
    """Return the other player, for a player WHO numbered 0 or 1.

    >>> other(0)
    1
    >>> other(1)
    0
    """
    return 1 - who

def play(strategy0, strategy1, score0=0, score1=0, goal=GOAL_SCORE):
    """Simulate a game and return the final scores of both players, with
    Player 0's score first, and Player 1's score second.

    A strategy is a function that takes two total scores as arguments
    (the current player's score, and the opponent's score), and returns a
    number of dice that the current player will roll this turn.

    strategy0:  The strategy function for Player 0, who plays first
    strategy1:  The strategy function for Player 1, who plays second
    score0   :  The starting score for Player 0
    score1   :  The starting score for Player 1
    """

    # solution 1
    """
    who = 0  # Which player is about to take a turn, 0 (first) or 1 (second)
    "*** YOUR CODE HERE ***"
    while score0 < GOAL_SCORE and score1 < GOAL_SCORE:
        if who == 0:
            num0 = strategy0(score0, score1)
            dice0 = select_dice(score0, score1)
            score0 += take_turn(num0, score1, dice0)
        else:
            num1 = strategy1(score1, score0)
            dice1 = select_dice(score1, score0)
            score1 += take_turn(num1, score0, dice1)

        if score0 == 2 * score1 or score1 == 2 * score0:
            tmp = score0
            score0 = score1
            score1 = tmp

        who = other(who)

    return score0, score1  # You may want to change this line.
    """

    def play_helper(strategy, opponent_strategy, score, opponent_score):
        if score >= 100 or opponent_score >= 100:
            return score, opponent_score

        num = strategy(score, opponent_score)
        dice = select_dice(score, opponent_score)
        score += take_turn(num, opponent_score, dice)

        # Swine swap rule
        if score == 2 * opponent_score or opponent_score == 2 * score:
            tmp = opponent_score
            opponent_score = score
            score = tmp

        opponent_score, score = play_helper(opponent_strategy, strategy, opponent_score, score)
        return score, opponent_score

    return play_helper(strategy0, strategy1, score0, score1)
            

#######################
# Phase 2: Strategies #
#######################

def always_roll(n):
    """Return a strategy that always rolls N dice.

    A strategy is a function that takes two total scores as arguments
    (the current player's score, and the opponent's score), and returns a
    number of dice that the current player will roll this turn.

    >>> strategy = always_roll(5)
    >>> strategy(0, 0)
    5
    >>> strategy(99, 99)
    5
    """
    def strategy(score, opponent_score):
        return n
    return strategy

# Experiments

def make_averaged(fn, num_samples=10000):
    """Return a function that returns the average_value of FN when called.

    To implement this function, you will have to use *args syntax, a new Python
    feature introduced in this project.  See the project description.

    >>> dice = make_test_dice(3, 1, 5, 6)
    >>> averaged_dice = make_averaged(dice, 1000)
    >>> averaged_dice()
    3.75
    >>> make_averaged(roll_dice, 1000)(2, dice)
    6.0

    In this last example, two different turn scenarios are averaged.
    - In the first, the player rolls a 3 then a 1, receiving a score of 1.
    - In the other, the player rolls a 5 and 6, scoring 11.
    Thus, the average value is 6.0.
    """
    def helper(*args):
        total, i = 0, 0
        while i < num_samples:
            total += fn(*args)
            i += 1
        return total / num_samples

    return helper


def max_scoring_num_rolls(dice=six_sided):
    """Return the number of dice (1 to 10) that gives the highest average turn
    score by calling roll_dice with the provided DICE.  Assume that dice always
    return positive outcomes.

    >>> dice = make_test_dice(3)
    >>> max_scoring_num_rolls(dice)
    10
    """
    max_average, num_rolls_with_max_average = 0, 1
    i = 1
    while i <= 10:
        average = make_averaged(roll_dice)(i, dice)
        if average > max_average:
            max_average = average
            num_rolls_with_max_average = i
        i += 1

    return num_rolls_with_max_average


def winner(strategy0, strategy1):
    """Return 0 if strategy0 wins against strategy1, and 1 otherwise."""
    score0, score1 = play(strategy0, strategy1)
    if score0 > score1:
        return 0
    else:
        return 1

def average_win_rate(strategy, baseline=always_roll(5)):
    """Return the average win rate (0 to 1) of STRATEGY against BASELINE."""
    win_rate_as_player_0 = 1 - make_averaged(winner)(strategy, baseline)
    win_rate_as_player_1 = make_averaged(winner)(baseline, strategy)
    return (win_rate_as_player_0 + win_rate_as_player_1) / 2 # Average results

def run_experiments():
    """Run a series of strategy experiments and report results."""
    if True: # Change to False when done finding max_scoring_num_rolls
        six_sided_max = max_scoring_num_rolls(six_sided)
        print('Max scoring num rolls for six-sided dice:', six_sided_max)
        average_six_sided = make_averaged(roll_dice)(six_sided_max, six_sided)
        print('Average score:', make_averaged(roll_dice)(six_sided_max, six_sided))
        print('5 rolls score:', make_averaged(roll_dice)(5, six_sided))

        four_sided_max = max_scoring_num_rolls(four_sided)
        print('Max scoring num rolls for four-sided dice:', four_sided_max)
        average_four_sided = make_averaged(roll_dice)(four_sided_max, four_sided)
        print('Average score:', make_averaged(roll_dice)(four_sided_max, four_sided))
        print('5 rolls score:', make_averaged(roll_dice)(5, four_sided))


    if True: # Change to True to test always_roll(8)
        print('always_roll(8) win rate:', average_win_rate(always_roll(8)))

    if True: # Change to True to test bacon_strategy
        print('bacon_strategy win rate:', average_win_rate(bacon_strategy))

    if True: # Change to True to test swap_strategy
        print('swap_strategy win rate:', average_win_rate(swap_strategy))

    if True: # Change to True to test final_strategy
        print('final_strategy win rate:', average_win_rate(final_strategy))

    "*** You may add additional experiments as you wish ***"

# Strategies

def bacon_strategy(score, opponent_score, margin=8, num_rolls=5):
    """This strategy rolls 0 dice if that gives at least MARGIN points,
    and rolls NUM_ROLLS otherwise.
    """
    "*** YOUR CODE HERE ***"
    bacon_score = 1 + abs((opponent_score // 10) - (opponent_score % 10))

    if bacon_score >= margin:
        return 0
    else:
        return num_rolls


def swap_strategy(score, opponent_score, margin=8, num_rolls=5):
    """This strategy rolls 0 dice when it would result in a beneficial swap and
    rolls NUM_ROLLS if it would result in a harmful swap. It also rolls
    0 dice if that gives at least MARGIN points and rolls
    NUM_ROLLS otherwise.
    """
    "*** YOUR CODE HERE ***"
    zero_score = take_turn(0, opponent_score)
    if (2 * (score + zero_score)) == opponent_score: # benifical swap
        return 0
    elif (score + zero_score) == 2 * opponent_score: # harmful swap
        return num_rolls
    else:
        return bacon_strategy(score, opponent_score, margin, num_rolls)


def final_strategy(score, opponent_score):
    """Write a brief description of your final strategy.

    *** YOUR DESCRIPTION HERE ***
    """
    six_sided_max = 6
    four_sided_max = 4
    average_six_sided = 8.7
    average_four_sided = 4.5

    max_num_rolls = 10

    def choose_four_sided(score):
        return score % 7 == 0

    is_four_sided = choose_four_sided(score + opponent_score)

    if is_four_sided:
        best_rolls = four_sided_max
        best_average = average_four_sided
    else:
        best_rolls = six_sided_max
        best_average = average_six_sided

    zero_score = take_turn(0, opponent_score)

    if (2 * (score + zero_score)) == opponent_score and zero_score >= best_average: # benifical swap
        return 0

    if (2 * (score + 1)) == opponent_score: # try to get benifical swap
        return max_num_rolls                # max_num_rolls have the largest probability to get 1.

    if (score + zero_score) == 2 * opponent_score: # harmful swap
        return best_rolls

    if choose_four_sided(score + zero_score + opponent_score) and zero_score >= (average_six_sided - average_four_sided): # make next opponent turn to get four-sided dice
        return 0

    return bacon_strategy(score, opponent_score, margin=best_average, num_rolls=best_rolls)


##########################
# Command Line Interface #
##########################

# Note: Functions in this section do not need to be changed.  They use features
#       of Python not yet covered in the course.


@main
def run(*args):
    """Read in the command-line argument and calls corresponding functions.

    This function uses Python syntax/techniques not yet covered in this course.
    """
    import argparse
    parser = argparse.ArgumentParser(description="Play Hog")
    parser.add_argument('--run_experiments', '-r', action='store_true',
                        help='Runs strategy experiments')
    args = parser.parse_args()

    if args.run_experiments:
        run_experiments()
