# CS 61A Fall 2014
# Name:
# Login:


def two_equal(a, b, c):
    """Return whether exactly two of the arguments are equal and the
    third is not.

    >>> two_equal(1, 2, 3)
    False
    >>> two_equal(1, 2, 1)
    True
    >>> two_equal(1, 1, 1)
    False
    >>> result = two_equal(5, -1, -1) # return, don't print
    >>> result
    True

    """
    return (a == b and a != c) or (a != b and (a == c or b == c))


def same_hailstone(a, b):
    """Return whether a and b are both members of the same hailstone
    sequence.

    >>> same_hailstone(10, 16) # 10, 5, 16, 8, 4, 2, 1
    True
    >>> same_hailstone(16, 10) # order doesn't matter
    True
    >>> result = same_hailstone(3, 19) # return, don't print
    >>> result
    False

    """
    def find(start, target):
        head = start
        while head != 1:
            if head == target:
                return True

            if head % 2 == 0:
                head = head / 2
            else:
                head = head * 3 + 1
        return False

    return find(a, b) or find(b, a)


from operator import truediv

def trace(f):
    def inner(*args, **kwargs):
        res = f(*args, **kwargs)
        print res
        return res

    return inner

def near_golden(perimeter):
    """Return the integer height of a near-golden rectangle with PERIMETER.

    >>> near_golden(42) # 8 x 13 rectangle has perimeter 42
    8
    >>> near_golden(68) # 13 x 21 rectangle has perimeter 68
    13
    >>> result = near_golden(100) # return, don't print
    >>> result
    19

    """
    def diff(h, w):
        return abs(truediv(h, w) - (truediv(w, h) - 1.0))

    h, w, near_h = 1, (perimeter / 2 - 1), 1
    last_diff = diff(h, w)

    while w > 1:
        h, w = h + 1, w - 1
        new_diff = diff(h, w)
        if new_diff < last_diff:
            last_diff = new_diff
            near_h = h

    return near_h


        


